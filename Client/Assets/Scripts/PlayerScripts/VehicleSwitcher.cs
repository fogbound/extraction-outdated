﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VehicleSwitcher : MonoBehaviour {

    public GameObject vehicleObject;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        if (Input.GetKeyDown(KeyCode.F) && vehicleObject != null) {
            transform.parent = vehicleObject.transform;
            GameManager.GetInstance().playerInVehicle = true;
            GameManager.GetInstance().vehicle = vehicleObject;
            //  vehicleObject.GetComponent<UnityStandardAssets.Vehicles.Car.CarUserControl>().enabled = true;
            //     vehicleObject.GetComponent<UnityStandardAssets.Vehicles.Car.CarController>().carCamera.enabled = true;
            gameObject.SetActive(false);
        }
    }

    private void OnTriggerEnter (Collider other) {
        if (other.transform.parent.tag == "Vehicle") {
            vehicleObject = other.transform.parent.gameObject;
        }
    }

    private void OnTriggerExit (Collider other) {
        if (other.transform.parent.tag == "Vehicle") {
            vehicleObject = null;
            GameManager.GetInstance().vehicle = null;
        }
    }
}
