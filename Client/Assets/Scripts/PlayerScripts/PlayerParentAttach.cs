﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerParentAttach : MonoBehaviour {

    public Transform playerModel;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        transform.position = playerModel.position;
	}
}
