﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Rewired;

public class CameraController : MonoBehaviour
{
    [SerializeField]
    private GameObject m_CameraRotationParent;
    public GameObject m_PlayerObj;

    [SerializeField]
    private GameObject m_Camera;

    [SerializeField]
    private GameObject m_CameraOrigin;

    private Player m_Player;
    public int m_PlayerID = 0;
    private CharacterController m_CharCon;

    float time = 0;
    float maxtime = 2;

    public float minClampedHeight = -80f;
    public float maxClampedHeight = 50f;
    public float clampXPos = 0f;
    public float rotationDampening = 2f;

    void Awake()
    {
        m_Player = ReInput.players.GetPlayer(m_PlayerID);
        m_CharCon = GetComponent<CharacterController>();
        time = maxtime;
    }
    void Update()
    {

        UpdateReticle();

        //gets the input on the mouse axis
        float mouseYPos = m_Player.GetAxis("HorizontalLook") * 1 * GameManager.GetInstance().mouseSensitivity;
        float mouseXPos = m_Player.GetAxis("VerticalLook") * 1 * GameManager.GetInstance().mouseSensitivity;

        clampXPos -= mouseXPos;

        // clamps the y axis movement. doesn't work :D
        // float clampedXPos = Mathf.Clamp(mouseXPos, -50, 60);
        // This doesn't work because [mouseXPos] returns 0f-1f, and will be clamped to the 0f-1f range (as it resides >-50f and <60f)
        // so basically you're saying [clampedXPos = mouseXPos] here
        // passing in a small value to the transform rotation will do basically nothing

        if (clampXPos < minClampedHeight)
            clampXPos = minClampedHeight;
        else if (clampXPos > maxClampedHeight)
            clampXPos = maxClampedHeight;

        //rotates axis individually.
        // m_CameraRotationParent.transform.Rotate(-clampXPos * Vector3.right, Space.Self);
        // also this line made shit weird
        m_CameraRotationParent.transform.localRotation = Quaternion.Euler(clampXPos * Vector3.right); // this one seems to work fine
        m_CameraRotationParent.transform.Rotate(mouseYPos * Vector3.up, Space.World);

        // freelook stuff
        if (m_PlayerObj != null) {
            if (!m_Player.GetButton("Freelook")) {
                m_PlayerObj.transform.Rotate(mouseYPos * Vector3.up, Space.World);
                // m_PlayerObj.transform.rotation = Quaternion.Slerp(m_PlayerObj.transform.rotation, Quaternion.Euler(m_PlayerObj.transform.rotation.eulerAngles.x, mouseYPos, m_PlayerObj.transform.rotation.eulerAngles.z), Time.deltaTime / rotationDampening);
            }
            if (m_Player.GetButtonUp("Freelook")) {
                // slerps camera to behind the player again
                Quaternion lookOnLook = Quaternion.LookRotation(m_CameraRotationParent.transform.position - m_CameraOrigin.transform.position);
                Debug.Log("stopped freelook");
                m_CameraRotationParent.transform.rotation = Quaternion.Slerp(m_CameraRotationParent.transform.rotation, lookOnLook, Time.deltaTime);
            }
        }
    }

    private void UpdateReticle()
    {
        RaycastHit hit;
        if (Physics.Raycast(Camera.main.transform.position, Camera.main.transform.forward, out hit))
        {
            if (hit.collider.tag == "Player")
            {
                PlayerUIController.GetInstance().SeeEnemy();

            }

            else
                PlayerUIController.GetInstance().ResetReticleColor();
        }
    }
}
