﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerUIController : MonoBehaviour {

    private static PlayerUIController instance;

    public Text ammoText;
    public Text hpText;
    public Text playersAlive;
    public Text gameInstructorText;
    public Image reticle;
    public Image pingIndicator;

    public GameObject aliveContainer;
    public GameObject healthContainer;
    public GameObject ammoContainer;

    private bool gameMessageDisplayTimeSet = false;
    private float displayTimeLeft = 0f;

    private void Awake () {
        if (instance == null)
            instance = this;
    }

    private void Update () {
        if (ExampleClient.GetInstance().gameState == ExampleClient.GameState.PreGame) {
            SetGameMessage("Match starts in " + GameManager.GetInstance().pregameWaitTime + " seconds!");
        }

        if (gameMessageDisplayTimeSet) {
            displayTimeLeft -= Time.deltaTime;
            if (displayTimeLeft <= 0f) {
                SetGameMessage("");
                gameMessageDisplayTimeSet = false;
            }
        }
    }

    public static PlayerUIController GetInstance () { return instance; }

    public void ToggleAmmoUI () { if (ammoContainer != null) ammoContainer.SetActive(!ammoContainer.activeSelf); }
    public void ToggleHealthUI () { if (healthContainer != null) healthContainer.SetActive(!healthContainer.activeSelf); }
    public void ToggleAliveUI () { if (aliveContainer != null) aliveContainer.SetActive(!aliveContainer.activeSelf); }
    public void ToggleAmmoUI (bool vis) { if (ammoContainer != null) ammoContainer.SetActive(vis); }
    public void ToggleHealthUI (bool vis) { if (healthContainer != null) healthContainer.SetActive(vis); }
    public void ToggleAliveUI (bool vis) { if (aliveContainer != null) aliveContainer.SetActive(vis); }

    public void SetAmmoCount (int magAmmo, int reserveAmmo) {
        ammoText.text = magAmmo.ToString();
    }

    public void SetHealth (int healthAmount) {
        hpText.text = healthAmount.ToString();
    }

    public void SetPlayersAlive (int numPlayersAlive) {
        playersAlive.text = numPlayersAlive.ToString();
    }

    public void SetGameMessage (string message) {
        gameMessageDisplayTimeSet = false;
        gameInstructorText.text = message;
    }
    public void SetGameMessage (string message, float displayTime) {
        displayTimeLeft = displayTime;
        gameInstructorText.text = message;
        gameMessageDisplayTimeSet = true;
    }

    public void SetReticleColor (Color c) {
        reticle.color = c;
    }

    public void ResetReticleColor () {
        SetReticleColor(Color.white);
    }

    public void SeeEnemy () {
        SetReticleColor(Color.red);
    }

    public void SeeSquadmate () {
        SetReticleColor(Color.cyan);
    }
}
