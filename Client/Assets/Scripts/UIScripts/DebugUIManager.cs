﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DebugUIManager : MonoBehaviour {

    private static DebugUIManager instance;
    private void Awake () {
        if (instance == null)
            instance = this;
    }
    public static DebugUIManager GetInstance () { return instance; }

    public float pingInterval = 2f;
    public Text pingUI;

    private float pingIntTime;
    private bool shouldPing = false;

    private float firstPing = 0;
    private float secondPing = 0;
    private float pingTime = 0;

    private void Start () {
        pingIntTime = pingInterval;
    }

    private void Update () {
        
        if (!shouldPing) {
            pingIntTime -= Time.deltaTime;
            if (pingIntTime <= 0f) {
                // do ping
                firstPing = Time.realtimeSinceStartup;
                shouldPing = true;
            }
        }
        else {
            // do the pinging
            ExampleClient.GetInstance().clientNet.CallRPC("Ping", UCNetwork.MessageReceiver.ServerOnly, -1);
        }

    }

    public void UpdatePing() {
        secondPing = Time.realtimeSinceStartup;
        pingTime = secondPing - firstPing;
        pingIntTime = pingInterval;
        pingUI.text = "Ping: " + pingTime.ToString() + "ms";
        shouldPing = false;
    }

}