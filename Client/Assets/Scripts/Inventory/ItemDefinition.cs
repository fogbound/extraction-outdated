﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "NewItemDefinition", menuName = "Items/Create Item Definition")]
public class ItemDefinition : ScriptableObject
{
    public enum EquipmentType
    {
        Weapon,
        Helmet,
        Armor,
        Backpack,
        MeleeWeapon,
    }

    public GameObject m_Visuals;
    public string m_ItemName;
    public int m_Weight;
    public string m_Description;
    public int m_MaxStackSize;
    public EquipmentType m_EquipmentType = new EquipmentType();
    public int m_MaxDurability;
    public bool m_Consumable;
    public int m_ItemID;
}
