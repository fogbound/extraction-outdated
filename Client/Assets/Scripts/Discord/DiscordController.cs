﻿using UnityEngine;

[System.Serializable]
public class DiscordJoinEvent : UnityEngine.Events.UnityEvent<string> { }

[System.Serializable]
public class DiscordSpectateEvent : UnityEngine.Events.UnityEvent<string> { }

[System.Serializable]
public class DiscordJoinRequestEvent : UnityEngine.Events.UnityEvent<DiscordRPC.JoinRequest> { }

public class DiscordController : MonoBehaviour {
    public DiscordRPC.RichPresence presence;
    public string appID;
    public string steamID;
    public int callbackCalls;
    public int clickCounter;
    public DiscordRPC.JoinRequest joinRequest;
    public UnityEngine.Events.UnityEvent onConnect;
    public UnityEngine.Events.UnityEvent onDisconnect;
    public UnityEngine.Events.UnityEvent hasResponded;
    public DiscordJoinEvent onJoin;
    public DiscordJoinEvent onSpectate;
    public DiscordJoinRequestEvent onJoinRequest;

    DiscordRPC.EventHandlers handlers;

    private static DiscordController instance;

    private void Awake () {
        if (instance == null)
            instance = this;

        DontDestroyOnLoad(gameObject);
    }

    public static DiscordController GetInstance() { return instance; }

    public void OnClick () {
        clickCounter++;
        presence.details = string.Format("Button clicked {0} times!", clickCounter.ToString());
        DiscordRPC.UpdatePresence(ref presence);
    }

    public void UpdatePresence () {
        DiscordRPC.UpdatePresence(ref presence);
    }

    public void RequestRespondYes () {
        Debug.Log("DiscordRequest :: respond yes");
        DiscordRPC.Respond(joinRequest.userId, DiscordRPC.Reply.Yes);
    }

    public void RequestRespondNo () {
        Debug.Log("DiscordRequest :: respond no");
        DiscordRPC.Respond(joinRequest.userId, DiscordRPC.Reply.No);
    }

    public void ReadyCallback () {
        ++callbackCalls;
        Debug.Log("Discord ready!");
        onConnect.Invoke();
    }

    public void DisconnectCallback (int errorCode, string message) {
        ++callbackCalls;
        Debug.Log("Discord disconnect " + errorCode.ToString() + ": " + message);
        onDisconnect.Invoke();
    }

    public void ErrorCallback (int errorCode, string message) {
        ++callbackCalls;
        Debug.LogError("Discord error " + errorCode.ToString() + ": " + message);
    }

    public void JoinCallback (string secret) {
        ++callbackCalls;
        Debug.Log("Discord join: " + secret);
        onJoin.Invoke(secret);
    }

    public void SpectateCallback (string secret) {
        ++callbackCalls;
        Debug.Log("Discord spectate: " + secret);
        onSpectate.Invoke(secret);
    }

    public void RequestCallback (ref DiscordRPC.JoinRequest request) {
        ++callbackCalls;
        Debug.Log(string.Format("Discord join request {0}#{1}: {2}", request.username, request.discriminator, request.userId));
        joinRequest = request;
        onJoinRequest.Invoke(request);
    }

    private void Update () {
        DiscordRPC.RunCallbacks();
    }

    private void OnEnable () {
        Debug.Log("Discord init");
        callbackCalls = 0;
        handlers = new DiscordRPC.EventHandlers();
        handlers.readyCallback = ReadyCallback;
        handlers.disconnectedCallback += DisconnectCallback;
        handlers.errorCallback += ErrorCallback;
        handlers.joinCallback += JoinCallback;
        handlers.spectateCallback += SpectateCallback;
        handlers.requestCallback += RequestCallback;
        DiscordRPC.Initialize(appID, ref handlers, true, steamID);
    }

    private void OnDisable () {
        Debug.Log("Discord shutdown");
        presence.state = "";
        presence.details = "";
        presence.largeImageKey = "";
        presence.largeImageText = "";
        presence.partyMax = 0;
        presence.partySize = 0;
        presence.smallImageKey = "";
        presence.smallImageText = "";
        presence.startTimestamp = 0;
        presence.endTimestamp = 0;
        UpdatePresence();
        DiscordRPC.Shutdown();
    }
}
