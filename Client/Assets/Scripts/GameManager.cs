﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using SimpleJSON;
using System.IO;

using Steamworks;

public class GameManager : MonoBehaviour {

    [Header("GameConfig Settings")]
    [SerializeField]
    string gameConfigFile = "gameconfig.json";
    string gameConfigPath;

    [Header("Network Settings")]
    public string ipAddress;
    public int serverPort;
    public string localPlayerName;

    // Player information
    public int magAmmoCount;
    public int reserveAmmoCount;
    public int playerHealth;

    public PlayerUIController plyUI;

    // Pregame
    public int pregameWaitTime = 0;
    public float pregameWaitTimeLeft;

    [Header("Main Menu Settings")]
    public GameObject partyMenuList;
    public GameObject partyMemberInfoPrefab;
    [System.Serializable]
    public class PartyMember {
        public string username;
        public string userLevel;
        public bool userIsReady;
        public CSteamID userSteamID;
        public GameObject partyMemberInfoPanel;
        public PartyMember (CSteamID id, string name, string level, bool ready) { userSteamID = id; username = name; userLevel = level; userIsReady = ready; }
    }
    public List<PartyMember> partyMembers = new List<PartyMember>();

    // Steam Matchmaking Lobby
    public CSteamID steam_LobbyID;
    protected Callback<LobbyCreated_t> steam_LobbyCreated;
    protected Callback<LobbyMatchList_t> steam_LobbyList;
    protected Callback<LobbyEnter_t> steam_LobbyEnter;
    protected Callback<LobbyDataUpdate_t> steam_LobbyUpdate;
    protected Callback<LobbyKicked_t> steam_LobbyKicked;
    private CSteamID[] steam_LobbyMembers;


    [Header("UI Settings")]
    public Text mainMenuStatusText;
    public GameObject loadingScreen;
    public LoadingScreen lsManager;
    public string[] loadingMessages;

    [Header("Input Settings")]
    public float mouseSensitivity = 1f;
    public bool useUnifiedInput = false;
    public bool inventoryOpen = false;
    public GameObject inventoryUI;

    public bool playerInVehicle = false;
    public GameObject playerObject;
    public GameObject vehicle;
    public bool playerAlive = true;

    private static GameManager m_Instance;

    public JSONNode gameConfig;

    [System.Serializable]
    private class PlayerData {
        public string playerName;
    }



    private bool shouldChangeArea = true;

    private void Awake () {
        // SteamAPI.Init();
        if (m_Instance == null) {
            m_Instance = this;
            DontDestroyOnLoad(gameObject);
        }
        else
            DestroyImmediate(gameObject);

        gameConfigPath = gameConfigFile;
    }

    public static GameManager GetInstance () {
        return m_Instance;
    }

    private void Start () {

        if (SteamAPI.Init()) {
            Debug.Log("SteamAPI Initialized!");
        }
        else {
            Debug.Log("SteamAPI failed to initialize!");
            SteamAPI.RestartAppIfNecessary(new AppId_t(771030));
        }

        // playerObject = GameObject.FindGameObjectWithTag("Player");
        // inventoryUI = (inventoryUI == null) ? GameObject.Find("InventoryUI") : inventoryUI;
        // inventoryOpen = inventoryUI.activeSelf;
        //PlayerPrefs.SetString("PlayerName", null);

        plyUI = PlayerUIController.GetInstance();

        if (loadingScreen)
            loadingScreen.SetActive(false);

        if (SteamManager.Initialized) {
            // Set local player name to their Steam persona name
            localPlayerName = SteamFriends.GetPersonaName();
            // mainMenuStatusText.text = "connected user: " + localPlayerName + " / pre-alpha build";

            // Give them an achivement for starting the game for the first time
            bool ACH_OPEN_GAME;
            SteamUserStats.GetAchievement("OPEN_GAME", out ACH_OPEN_GAME);
            if (!ACH_OPEN_GAME) {
                SteamUserStats.SetAchievement("OPEN_GAME");
                SteamUserStats.StoreStats();
            }

            SteamFriends.SetRichPresence("status", "Chillin\' on the main menu");

            // Steam Lobby Matchmaking

            // setup callbacks
            steam_LobbyCreated = Callback<LobbyCreated_t>.Create(OnLobbyCreated);
            steam_LobbyList = Callback<LobbyMatchList_t>.Create(OnLobbyList);
            steam_LobbyUpdate = Callback<LobbyDataUpdate_t>.Create(OnLobbyInfo);
            steam_LobbyEnter = Callback<LobbyEnter_t>.Create(OnLobbyEnter);
            steam_LobbyKicked = Callback<LobbyKicked_t>.Create(OnLobbyKicked);

            // Create new lobby with up for 4 players max
            SteamAPICall_t steamAPI_CreateLobby = SteamMatchmaking.CreateLobby(ELobbyType.k_ELobbyTypePublic, 4);
        }

        Debug.Log("Loading GameConfig " + gameConfigPath);
        gameConfig = JSON.Parse(File.ReadAllText(gameConfigPath));
        if (gameConfig != null) {
            Debug.Log("GameConfig:\n" + gameConfig.ToString());
            ipAddress = gameConfig["serverconfig"]["ip"];
            serverPort = int.Parse(gameConfig["serverconfig"]["port"]);
        }
        else
            Debug.LogError("Error loading game configuration!");




        // update discord presence
        DiscordController.GetInstance().presence.state = "Idle";
        DiscordController.GetInstance().presence.details = "Chillin\' on the main menu";
        DiscordController.GetInstance().presence.startTimestamp = System.Convert.ToInt64(System.DateTime.Now.Second);
        DiscordController.GetInstance().UpdatePresence();

    }

    private void Update () {

        SteamAPI.RunCallbacks();

        if (plyUI == null)
            plyUI = PlayerUIController.GetInstance();

        if (shouldChangeArea && SceneManager.GetActiveScene() == SceneManager.GetSceneByBuildIndex(1)) {
            ExampleClient.GetInstance().clientNet.AddToArea(1);
            shouldChangeArea = false;
        }

        if (ExampleClient.GetInstance().gameState == ExampleClient.GameState.PreGameWaitForPlayers) {
            if (plyUI != null) {
                plyUI.ToggleAliveUI(false); plyUI.ToggleHealthUI(false); plyUI.ToggleAmmoUI(false);
                plyUI.SetGameMessage("Waiting for players");
            }
        }
        else if (ExampleClient.GetInstance().gameState == ExampleClient.GameState.PreGame) {
            pregameWaitTimeLeft -= Time.deltaTime;
            // Debug.ClearDeveloperConsole();
            // Debug.Log(pregameWaitTimeLeft);
            if (plyUI != null) {
                plyUI.SetGameMessage("Match starts in " + (int) pregameWaitTimeLeft + " seconds!");
                plyUI.ToggleAliveUI(true); plyUI.ToggleHealthUI(false);
            }
        }
        else if (ExampleClient.GetInstance().gameState == ExampleClient.GameState.GameActive) {
            if (plyUI != null) {
                plyUI.ToggleAliveUI(true);
                plyUI.ToggleHealthUI(true);
            }
        }
    }

    public void ToggleInventory () {
        inventoryOpen = !inventoryOpen;
        inventoryUI.SetActive(inventoryOpen);
    }

    public void AddPartyMember (CSteamID m_SteamID, string m_Username, string m_UserLevel, bool m_IsReady) {
        PartyMember p = new PartyMember(m_SteamID, m_Username, m_UserLevel, m_IsReady) {
            partyMemberInfoPanel = Instantiate(partyMemberInfoPrefab, partyMenuList.transform, false)
        };
        p.partyMemberInfoPanel.GetComponent<PartyMemberInfo>().SetUsername(m_Username);
        // p.partyMemberInfoPanel.GetComponent<PartyMemberInfo>().SetUserLevel(m_UserLevel);
        p.partyMemberInfoPanel.GetComponent<PartyMemberInfo>().SetReadyState(m_IsReady);
        p.partyMemberInfoPanel.GetComponent<PartyMemberInfo>().steamid = m_SteamID;

        // show/hide info panel buttons
        if (p.userSteamID != SteamUser.GetSteamID()) {
            if (SteamManager.Initialized && SteamMatchmaking.GetLobbyOwner(steam_LobbyID) != SteamUser.GetSteamID()) {
                p.partyMemberInfoPanel.GetComponent<PartyMemberInfo>().SetButtonVisibility("kick", false);
            }
            p.partyMemberInfoPanel.GetComponent<PartyMemberInfo>().SetButtonVisibility("ready", false);
        }

        partyMembers.Add(p);

        Debug.Log("AddPartyMember called -- Username: " + m_Username + " (SteamID: " + m_SteamID.ToString() + ")");
    }
    public void RemovePartyMember (CSteamID m_SteamID) {
        foreach (PartyMember p in partyMembers) {
            if (p.userSteamID == m_SteamID) {
                partyMembers.Remove(p);
                break;
            }
        }
    }
    public PartyMember GetPartyMember (CSteamID m_SteamID) {
        PartyMember pm = null;
        foreach (PartyMember p in partyMembers) {
            if (p.userSteamID == m_SteamID) {
                pm = p;
                break;
            }
        }
        return pm;
    }







    // Show loading screen
    public void ShowLoadingScreen (string loadStatus = "Now Loading...") {
        if (loadingScreen) {
            lsManager.SetLoadStatus(loadStatus);
            lsManager.SetTipText(loadingMessages[Random.Range(0, loadingMessages.Length)]);
            loadingScreen.SetActive(true);
        }
    }





    // Steam API
    void OnLobbyCreated (LobbyCreated_t result) {
        if (result.m_eResult == EResult.k_EResultOK) {
            SteamMatchmaking.SetLobbyData((CSteamID) result.m_ulSteamIDLobby, "name", SteamFriends.GetPersonaName());
            // SteamMatchmaking.JoinLobby((CSteamID) result.m_ulSteamIDLobby);
            steam_LobbyID = (CSteamID) result.m_ulSteamIDLobby;
            Debug.Log(result.m_eResult.ToString());
            Debug.Log(string.Format("Successfully created new lobby with id {0}!", steam_LobbyID.ToString()));

            steam_LobbyMembers = new CSteamID[4];
        }
        else {
            Debug.Log("SteamMatchmaking.CreateLobby failed! Return message: " + result.m_eResult.ToString());
            if (result.m_eResult == EResult.k_EResultAccessDenied) {
                // we'll still add the user to the party list to make it look nice
                AddPartyMember(SteamUser.GetSteamID(), SteamFriends.GetPersonaName(), "1", false);

                // still update discord presence
                DiscordController.GetInstance().presence.state = "In a party";
                DiscordController.GetInstance().presence.details = "Chillin\' on the main menu";
                DiscordController.GetInstance().presence.partySize = 1;
                DiscordController.GetInstance().presence.partyMax = 1;
                DiscordController.GetInstance().UpdatePresence();
            }
        }
        // steam_LobbyMembers = new CSteamID[SteamMatchmaking.GetLobbyMemberLimit(steam_LobbyID)];
        // AddPartyMember(SteamUser.GetSteamID(), SteamFriends.GetPersonaName(), "1", false);
    }
    void OnLobbyList (LobbyMatchList_t result) {

    }
    void OnLobbyInfo (LobbyDataUpdate_t result) {
        Debug.Log(string.Format("success: {0}, lobbyid: {1}, memberid: {2}", result.m_bSuccess, result.m_ulSteamIDLobby, result.m_ulSteamIDMember));
        for (int i = 0; i < steam_LobbyMembers.Length; i++) {
            if (partyMembers[i] != null) {
                PartyMemberInfo info = partyMembers[i].partyMemberInfoPanel.GetComponent<PartyMemberInfo>();
                if (SteamMatchmaking.GetLobbyMemberData(steam_LobbyID, info.steamid, "name") != "") {
                    partyMembers[i].username = SteamMatchmaking.GetLobbyMemberData(steam_LobbyID, info.steamid, "name");
                }
            }
            else {
                CSteamID mSteamID = new CSteamID(result.m_ulSteamIDMember);
                AddPartyMember(mSteamID, SteamFriends.GetFriendPersonaName(mSteamID), "1", false);
            }
        }
    }
    void OnLobbyEnter (LobbyEnter_t result) {

        Debug.Log("SteamAPI -- OnLobbyEnter called >> LobbyID: " + result.m_ulSteamIDLobby.ToString());
        steam_LobbyID = (CSteamID) result.m_ulSteamIDLobby;

        Debug.Log(string.Format("EChatRoomEnterResponse: {0}", result.m_EChatRoomEnterResponse));
        if (result.m_EChatRoomEnterResponse == 1) {
            Debug.Log(string.Format("SteamAPI -- Lobby name: {0} (owned by SteamID {1})", SteamMatchmaking.GetLobbyData(steam_LobbyID, "name"), SteamMatchmaking.GetLobbyOwner(steam_LobbyID).ToString()));
            for (int i = 0; i < steam_LobbyMembers.Length; i++) {

                /*
                Debug.Log(i);
                Debug.Log(string.Format("Party Member {0}: {1} ({2})", i,
                    SteamFriends.GetFriendPersonaName(SteamMatchmaking.GetLobbyMemberByIndex(steam_LobbyID, i)),
                    SteamMatchmaking.GetLobbyMemberByIndex(steam_LobbyID, i).m_SteamID));
                */

                if (SteamMatchmaking.GetLobbyMemberByIndex(steam_LobbyID, i).m_SteamID != 0) {
                    steam_LobbyMembers[i] = SteamMatchmaking.GetLobbyMemberByIndex(steam_LobbyID, i);
                    if (GetPartyMember(steam_LobbyMembers[i]) == null) {
                        if (steam_LobbyMembers[i].m_SteamID != SteamUser.GetSteamID().m_SteamID)
                            AddPartyMember(steam_LobbyMembers[i], SteamFriends.GetFriendPersonaName(steam_LobbyMembers[i]), "1", false);
                        else
                            AddPartyMember(SteamUser.GetSteamID(), SteamFriends.GetPersonaName(), "1", false);
                    }
                }
            }

            // update discord presence
            DiscordController.GetInstance().presence.state = "In a party";
            // DiscordController.GetInstance().presence.details = "Chillin\' on the main menu";
            DiscordController.GetInstance().presence.partySize = SteamMatchmaking.GetNumLobbyMembers(steam_LobbyID);
            DiscordController.GetInstance().presence.partyMax = 4;
            DiscordController.GetInstance().presence.largeImageKey = "main_menu";
            DiscordController.GetInstance().presence.largeImageText = "Chillin\' on the main menu";
            DiscordController.GetInstance().UpdatePresence();
        }
        else {
            Debug.Log("Join lobby failed :c");
        }
    }

    void OnLobbyKicked (LobbyKicked_t result) {
        
    }
}
