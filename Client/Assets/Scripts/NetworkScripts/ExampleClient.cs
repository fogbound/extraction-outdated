﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using Steamworks;

public class ExampleClient : MonoBehaviour {
    public ClientNetwork clientNet;

    // Get the instance of the FlagshipClient
    static ExampleClient instance = null;

    // Are we in the process of logging into a server
    private bool loginInProcess = false;

    public GameObject loginScreen;

    // player info
    GameObject localPlayerObject;

    GameManager gm;
    PlayerUIController playerUI;
    List<Vector3> debugBulletImpacts = new List<Vector3>();

    public enum GameState {
        MainMenu,
        PreGameWaitForPlayers,
        PreGame,
        GameActive,
        GameEnd
    }
    public GameState gameState = GameState.MainMenu;







    public bool shouldPlayDeathAnim = false;

    // Get the FlagshipClient
    public static ExampleClient GetInstance () {
        if (instance == null) {
            Debug.LogError("ExampleClient is uninitialized");
            return null;
        }
        return instance;
    }

    // Use this for initialization
    void Awake () {
        if (instance == null)
            instance = this;

        // Make sure we have a ClientNetwork to use
        if (clientNet == null) {
            clientNet = GetComponent<ClientNetwork>();
        }
        if (clientNet == null) {
            clientNet = (ClientNetwork) gameObject.AddComponent(typeof(ClientNetwork));
        }
    }

    void Start () {
        if (gm == null)
            gm = GameManager.GetInstance();
        if (playerUI == null)
            playerUI = PlayerUIController.GetInstance();
    }

    // Start the process to login to a server
    public void ConnectToServer () {
        if (loginInProcess) {
            return;
        }
        loginInProcess = true;

        ClientNetwork.port = GameManager.GetInstance().serverPort;
        clientNet.Connect(gm.ipAddress, ClientNetwork.port, gm.localPlayerName, "", "", 0);
        gm.ShowLoadingScreen("Joining server...");
    }

    public void ConnectToDS(string ipAddress, int port) {
        if (loginInProcess)
            return;

        loginInProcess = true;

        gm.ShowLoadingScreen(string.Format("Resolving {0}...", ipAddress));

        clientNet.Connect(ipAddress, port, gm.localPlayerName, "", "", 0);
    }
    public void ConnectToDS(string ipAddress) {
        ConnectToDS(ipAddress, 9200);
    }

    // Update is called once per frame
    void Update () {
        if (playerUI == null) {
            try {
                playerUI = PlayerUIController.GetInstance();
            }
            catch {

            }
        }
    }

    // Networking callbacks
    // These are all the callbacks from the ClientNetwork
    void OnNetStatusNone () {
        Debug.Log("OnNetStatusNone called");
    }
    void OnNetStatusInitiatedConnect () {
        Debug.Log("OnNetStatusInitiatedConnect called");
        gm.ShowLoadingScreen("Joining server...");
    }
    void OnNetStatusReceivedInitiation () {
        Debug.Log("OnNetStatusReceivedInitiation called");
    }
    void OnNetStatusRespondedAwaitingApproval () {
        Debug.Log("OnNetStatusRespondedAwaitingApproval called");
        gm.ShowLoadingScreen("Authenticating...");
    }
    void OnNetStatusRespondedConnect () {
        Debug.Log("OnNetStatusRespondedConnect called");
    }
    void OnNetStatusConnected () {
        // MainMenuController.GetInstance().SetLoadingText("Connected!", false);
        // loginScreen.SetActive(false);
        Debug.Log("OnNetStatusConnected called");

        DiscordController.GetInstance().presence.details = "Finding a match...";
        DiscordController.GetInstance().UpdatePresence();

        gm.ShowLoadingScreen("Loading world...");
        SceneManager.LoadSceneAsync("World");
        gameState = GameState.PreGame;
    }

    void OnNetStatusDisconnecting () {
        Debug.Log("OnNetStatusDisconnecting called");

        if (localPlayerObject)
            clientNet.Destroy(localPlayerObject.GetComponent<NetworkSync>().GetId());
    }
    void OnNetStatusDisconnected () {
        // MainMenuController.GetInstance().SetLoadingText("Play Solo", true);
        Debug.Log("OnNetStatusDisconnected called");
        if (SceneManager.GetActiveScene().name == "MainMenu") {
            gm.ShowLoadingScreen("Returning to main menu.");
            // gm.usernamePickerUI.SetActive(false);
            // gm.lsManager.ShowCharacterModel();
            // gm.loadingScreen.SetActive(false);
        }
        else {
            if (playerUI)
                playerUI.SetGameMessage("Connection closed, returning to main menu...");
        }

        SceneManager.LoadSceneAsync("MainMenu");

        loginInProcess = false;

        if (localPlayerObject)
            clientNet.Destroy(localPlayerObject.GetComponent<NetworkSync>().GetId());
    }
    public void OnChangeArea () {
        Debug.Log("OnChangeArea called");

        localPlayerObject = clientNet.Instantiate("NetSync_Player_Male_01", new Vector3(0, 0.5f, 0), Quaternion.identity);
        localPlayerObject.GetComponent<NetworkSync>().AddToArea(1);
        clientNet.CallRPC("UpdatePlayerID", UCNetwork.MessageReceiver.ServerOnly, -1, localPlayerObject.GetComponent<NetworkSync>().GetId());
        localPlayerObject.GetComponent<PlayerController>().playerEnabled = true;

        // localPlayerObject.GetComponent<GestureManager>().gestureMenu = GameObject.FindGameObjectWithTag("_UI_GestureMenu").GetComponent<RadialMenu>();
        // localPlayerObject.GetComponent<GestureManager>().tauntMenu = GameObject.FindGameObjectWithTag("_UI_TauntMenu").GetComponent<RadialMenu>();

        // DontDestroyOnLoad(localPlayerObject);

        // GameObject.FindGameObjectWithTag("TPCamHolder").GetComponent<TPCameraController>().playerTransform = localPlayerObject.transform;
        // GameObject.FindGameObjectWithTag("TPCamHolder").transform.parent = localPlayerObject.transform;
        GameObject.FindGameObjectWithTag("CamRotParent").transform.parent = localPlayerObject.transform;
        GameObject.FindGameObjectWithTag("CamRotParent").GetComponentInChildren<CameraController>().m_PlayerObj = localPlayerObject;
        Vector3 camOffset = new Vector3(0.5f, 1.5f, -0.9f);
        GameObject.FindGameObjectWithTag("CamOrigin").transform.parent = localPlayerObject.transform;
        GameObject.FindGameObjectWithTag("CamRotParent").GetComponentInChildren<CameraController>().transform.position = camOffset;
        GameObject.FindGameObjectWithTag("CamOrigin").transform.position = camOffset;
        Camera.main.transform.position = camOffset + new Vector3(0, 1, 0);
        Debug.Log("Instanciated player!");

        DiscordController.GetInstance().presence.state = "In-Game";
        DiscordController.GetInstance().presence.details = "Attempting to survive";
        DiscordController.GetInstance().presence.startTimestamp = long.Parse(System.DateTime.Now.ToString("ss"));
        DiscordController.GetInstance().UpdatePresence();
    }

    // RPC Called by the server once it has finished sending all area initization data for a new area
    public void AreaInitialized () {
        Debug.Log("AreaInitialized called");
    }

    void OnDestroy () {
        if (localPlayerObject)
            clientNet.Destroy(localPlayerObject.GetComponent<NetworkSync>().GetId());

        if (clientNet.IsConnected()) {
            clientNet.Disconnect("Peace out");
        }
    }







    // rpcs n stuff
    public void Pong () {
        DebugUIManager.GetInstance().UpdatePing();
    }

    public void SetNetworkID (int networkID) {
        if (localPlayerObject != null && localPlayerObject.GetComponent<PlayerController>())
            localPlayerObject.GetComponent<PlayerController>().clientNetworkId = networkID;
    }

    public void SetPlayerName (int networkID, string networkPlayerName) {
        GameObject[] connectedPlayers = GameObject.FindGameObjectsWithTag("Player");
        foreach (GameObject player in connectedPlayers) {
            if (player.GetComponent<PlayerController>()) {
                if (player.GetComponent<PlayerController>().clientNetworkId == networkID) {
                    player.GetComponent<PlayerController>().playerName = networkPlayerName;
                }
            }
        }
    }

    public void SetPlayerHealth (int healthAmount) {
        gm.playerHealth = healthAmount;
        if (playerUI == null) {
            playerUI = PlayerUIController.GetInstance();
            if (playerUI != null)
                playerUI.SetHealth(healthAmount);
        }
        else
            playerUI.SetHealth(healthAmount);
    }
    public void SetPlayerAmmoCount (int magAmmo, int reserveAmmo) {
        gm.magAmmoCount = magAmmo; gm.reserveAmmoCount = reserveAmmo;
        if (playerUI == null) {
            playerUI = PlayerUIController.GetInstance();
            if (playerUI != null)
                playerUI.SetAmmoCount(magAmmo, reserveAmmo);
        }
        else
            playerUI.SetAmmoCount(magAmmo, reserveAmmo);
    }

    public void SendGameMessage (string message) {
        if (playerUI) {
            playerUI.SetGameMessage(message);
        }
    }
    public void SendGameMessage (string message, float displayTime) {
        if (playerUI)
            playerUI.SetGameMessage(message, displayTime);
    }

    public void SetGameState (int aGameState) {
        gameState = (GameState) aGameState;
    }

    public void SetPregameWaitTime(int timeLeft) {
        gm.pregameWaitTime = timeLeft;
        gm.pregameWaitTimeLeft = timeLeft;
    }

    public void GameStart () {
        // do game start logic on client
        Debug.Log("Game start!");
        if (playerUI)
            playerUI.SetGameMessage("");
    }

    public void RespawnPlayer (Vector3 respawnPos) {
        localPlayerObject.transform.position = respawnPos;
    }

    // If the requested cast returned valid, run some code
    public void CastValid (string callback) {
        // SendMessage(callback);
        Debug.Log("CastValid");
    }

    // If the requested cast returned invalid, run some code
    public void CastInvalid (string callback) {
        // SendMessage(callback);
        Debug.Log("CastInvalid");
    }



    // Hit Detection RPCs
    // ShotHit RPC
    // Called from the server when the local client's RequestShot RPC is valid
    // Retrieves the point where the raycast determined the impact from the shot was
    public void ShotHit (Vector3 hitPoint, Vector3 hitNormal) {
        // Debug -- print out the location of the bullet impact
        // Debug.Log("Shot hit at " + hitPoint.ToString());
        // debugBulletImpacts.Add(hitPoint);

        // Instantiate bullet impact particle effect at hit point
        clientNet.Instantiate("Bullet_Impact", hitPoint, Quaternion.LookRotation(hitNormal));
    }

    public void PlayGunshot (int netId) {
        clientNet.GetGameObject(netId).GetComponent<AudioSource>().Play();
    }

    public void PlayerKilled (string victimName) {
        SendGameMessage(string.Format("You killed {0}!", victimName), 2f);

        if (SteamManager.Initialized) {
            bool ACH_PLAYER_KILLED;
            SteamUserStats.GetAchievement("PLAYER_KILLED", out ACH_PLAYER_KILLED);
            if (!ACH_PLAYER_KILLED) {
                SteamUserStats.SetAchievement("PLAYER_KILLED");
                SteamUserStats.StoreStats();
            }
        }
    }

    public void PlayerDeath (string attackerName, int attackerID) {
        Debug.Log("Killed by " + attackerName + "!");
        SendGameMessage(string.Format("You were killed by {0}!", attackerName), 2f);
        clientNet.Instantiate("Gibb", localPlayerObject.transform.position, Quaternion.identity);
        gm.playerAlive = false;
        shouldPlayDeathAnim = true;

        // spawn loot crate here
        // set camera parent to attacker (spectate)
        Camera.main.transform.parent = clientNet.GetGameObject(attackerID).transform;
        // destroy player object
        Destroy(localPlayerObject);
    }



    // Reload RPCs
    public void DoReload() {
        // play animation
        // update ui
    }

    private void OnDrawGizmos () {
        Gizmos.color = Color.blue;
        foreach (Vector3 shotPoint in debugBulletImpacts) {
            Gizmos.DrawCube(shotPoint, Vector3.one / 2);
        }
    }
}


