﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEditor.SceneManagement;
using UnityEngine;

public class SceneExporter : MonoBehaviour {
    [MenuItem("Assets/Export Scene to Server")]
    static void ExportSceneToServer () {
        EditorSceneManager.SaveOpenScenes();
        AssetDatabase.CopyAsset("Assets/Scenes/" + EditorSceneManager.GetActiveScene().path, "Assets/Scenes/Server_" + EditorSceneManager.GetActiveScene().path);
        string serverScene = "Server_" + EditorSceneManager.GetActiveScene().name;
        UnityEngine.SceneManagement.Scene activeScene = EditorSceneManager.GetActiveScene();
        UnityEngine.SceneManagement.Scene activeServerScene;
        EditorSceneManager.OpenScene("blank", OpenSceneMode.Single);
        if (EditorSceneManager.CloseScene(activeScene, false)) {
            EditorSceneManager.OpenScene(serverScene, OpenSceneMode.Single);
            foreach (Renderer rend in GameObject.FindObjectsOfType<Renderer>()) {
                Destroy(rend);
            }
            EditorSceneManager.SaveOpenScenes();
            activeServerScene = EditorSceneManager.GetActiveScene();
            EditorSceneManager.OpenScene("blank", OpenSceneMode.Single);
            if (EditorSceneManager.CloseScene(activeServerScene, false)) {
                AssetDatabase.MoveAsset("Assets/Scenes/" + serverScene + ".unity", "../Server/Assets/Scenes/" + serverScene);
            }
        }
    }
}