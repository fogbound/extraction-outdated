﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(AudioSource))]
public class SoundManager : MonoBehaviour {

    private static SoundManager instance;

    public AudioSource audio;

    private void Awake () {
        if (instance == null)
            instance = this;
        if (audio == null)
            audio = GetComponent<AudioSource>();
    }
    public static SoundManager GetInstance() { return instance; }

    public void PlaySound (AudioClip clip) {
        if (!audio.isPlaying) {
            audio.clip = clip;
            audio.Play();
        }
    }
}
