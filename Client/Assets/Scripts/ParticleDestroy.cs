﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParticleDestroy : MonoBehaviour {

    // Use this for initialization
    void Start () {
    }

    // Update is called once per frame
    void FixedUpdate () {
        if (!GetComponent<ParticleSystem>().IsAlive()) {
            Destroy(gameObject);
        }
    }
}
