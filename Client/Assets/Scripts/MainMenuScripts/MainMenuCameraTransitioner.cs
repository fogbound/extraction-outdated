﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainMenuCameraTransitioner : MonoBehaviour {

    public float transitionSpeed = 2f;
    public float distThreshold = 0.1f;

    [System.Serializable]
    public class CamLocations {
        public string locName;
        public Vector3 camPos;
        public Vector3 camRot;
    }

    public CamLocations[] camLocations;
    public int curLoc = 0;
    public int nextLoc = 0;

    private void Update() {
        /*
        if (nextLoc != curLoc) {
            transform.position = Vector3.Lerp(transform.position, camLocations[nextLoc].camPos, transitionSpeed * Time.deltaTime);
            // transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.Euler(camLocations[nextLoc].camRot), transitionSpeed * Time.deltaTime);

            if (Vector3.Distance(transform.position, camLocations[nextLoc].camPos) <= distThreshold)
                curLoc = nextLoc;
        }
        */

        transform.position = Vector3.Lerp(transform.position, camLocations[nextLoc].camPos, transitionSpeed * Time.deltaTime);
    }

    public void ChangeLocation(int newLoc) {
        nextLoc = newLoc;
    }
}
