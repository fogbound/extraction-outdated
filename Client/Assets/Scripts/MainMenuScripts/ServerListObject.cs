﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ServerListObject : MonoBehaviour {

    public string ipAddress;
    public int port;
    public string servername;
    public int playercount;
    public int maxplayers;
    public string gamestate;

    public Text serverInfo;

    // Use this for initialization
    void Start () {
        UpdateServerInfo();
    }

    // Update is called once per frame
    void Update () {

    }

    public void UpdateServerInfo () {
        serverInfo.text = servername + " [" + playercount + "/" + maxplayers + "]";
    }

    public void ConnectToServer () {
        ExampleClient.GetInstance().ConnectToDS(ipAddress, port);
    }
}
